resource "aws_acm_certificate" "app" {
  domain_name       = "${var.vpc_name}.example.su"
  validation_method = "DNS"
}
