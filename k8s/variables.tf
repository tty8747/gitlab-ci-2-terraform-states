variable "aws_access_key_id" {
  type = string
}

variable "aws_secret_access_key" {
  type = string
}

variable "cidr" {
  type        = string
  default     = "10.0.0.0/16"
  description = "CIDR for k8s vpc"
}

variable "region" {
  type    = string
  default = "eu-west-1"
}

variable "vpc_name" {
  type = string
  default = "k8s"
}
